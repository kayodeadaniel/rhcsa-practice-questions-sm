35

# Managing containers

## Question

- Pull Apache Web server image (httpd 2.4) and run the container with name `apache-container`
- Configure `apache-container` to display content “Hello Apache From Inside The Container”
- Configure the container to receive http requests at port 3200 in the host machine

## Background

Container management is one of the new objectives of the RHCSA exam, Linux containers have emerged as a key open source application packaging and delivery technology.

To simplify the concept, you could package an application in a container, for example, if you have a Java application, then you need to install Java first, imagine being able to get a pre-configured container that already contains all the dependencies needed to run this Java app in a single unit. 

A container runs logically in a pod, think of a pod as a wrapper to containers.

Container images reside in a registry, allowing you to push, pull, run... etc images from that registry, some of these registries could public and some of them are private require you to authenticate(username and password will be given in the exam).

In Linux, to be able to mange containers and images you need some tools: podman is used for directly managing pods and container images (run, stop, start, ps, attach, exec, and so on).

## Answer

1- As a root user, install podman

```console
[root@server1 ~]# yum module install -y container-tools
```

2- Search for the right container image

```console
[root@server1 ~]# podman search httpd-2.4
INDEX       NAME                                         DESCRIPTION                                       STARS   OFFICIAL   AUTOMATED
redhat.io   registry.redhat.io/rhscl/httpd-24-rhel7      Apache HTTP 2.4 Server                            0
redhat.io   registry.redhat.io/rhel8/httpd-24            Apache HTTP Server 2.4 available as containe...   0
quay.io     quay.io/sjenning/httpd                                                                         0
quay.io     quay.io/redhattraining/httpd-parent                                                            0
quay.io     quay.io/centos7/httpd-24-centos7                                                               0
```
As you could see, there are multiple images, these images are stored in different registries, for this question we will choose the second image: `registry.redhat.io/rhel8/httpd-24` 



3- Pull the container, but before that, we need to log-in to the container registry

```console
[root@server1 ~]# podman login registry.redhat.io
Authenticating with existing credentials...
Existing credentials are invalid, please enter valid username and password
Username: username
Password:
```
Note, the username and password will be provided in the exam, but if you have a redhat account, you could use the credentials of that account and you will be able to login

Now you can pull the container

```console
[root@server1 ~]# podman pull registry.redhat.io/rhel8/httpd-24
```

4- Create a webpage with the content:
```console
vim /var/www/html/index.html
```



and insert: “Hello Apache From Inside The Container”

then :wq to save change


5- Inspect the image to see what ports are exposed from the container.

The idea is, you want to map between a port from your host machine to your container, this httpd is working on a specific port in the container, you want to `forward` from the container to the host HOST_PORT:CONTAINER_PORT. 

We know the host port is `3200` as stated in the questions, now we need to know what is the container part: `ExposedPorts`

```
podman run --name apache-container -d -p 3200:EXPOSED_PORT -v /var/www/html:ORIG_DATA_PATH IMAGE_PATH
```


```console
[root@server1 ~]# podman inspect registry.redhat.io/rhel8/httpd-24
[
    {
        ...
        "Created": "2020-12-11T05:22:01.80474Z",
        "Config": {
            "User": "1001",
            "ExposedPorts": {
                "8080/tcp": {},
                "8443/tcp": {}
            },
            "Env": [
                "PATH=/opt/app-root/src/bin:/opt/app-root/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                "container=oci",
                ...
                "HTTPD_MAIN_CONF_D_PATH=/etc/httpd/conf.d",
                "HTTPD_TLS_CERT_PATH=/etc/httpd/tls",
                "HTTPD_VAR_RUN=/var/run/httpd",
                "HTTPD_DATA_PATH=/var/www",
                "HTTPD_DATA_ORIG_PATH=/var/www",
                "HTTPD_LOG_PATH=/var/log/httpd"
                ...
            ],

            }
    }
]

```

Look for `ExposedPorts` in the inspection, as you can see http is exposed at port `8080` and https is exposed at port `8443`.

6- Run the container:

```console
podman run --name apache-container -d -p 3200:8080 -v /var/www/html:/var/www/html registry.redhat.io/rhel8/httpd-24
```


7- Restart the apache server
```console
[root@server1 html]# podman restart apache-container
```

8- Verify that the page is working:
```console
[root@server1 html]# curl localhost:3200
```
